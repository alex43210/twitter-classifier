import pandas as pd
from importlib import import_module
from argparse import ArgumentParser
import json
from classifier import LabeledText


def _parse_args():
    parser = ArgumentParser()
    parser.add_argument("--classifier_class",
                        help="text classifier class name",
                        default="classifier.TweetClassifier")
    parser.add_argument("-dataset",
                        help="dataset path")
    parser.add_argument("-output_path",
                        help="output path")
    return parser.parse_args()


def _get_classifier_constructor(classifier_full_class_name):
    classifier_module_name = '.'.join(classifier_full_class_name.split('.')[:-1])
    classifier_class_name = classifier_full_class_name.split('.')[-1]
    classifier_module = import_module(classifier_module_name)
    classifier_class = getattr(classifier_module, classifier_class_name)
    return classifier_class


if __name__ == '__main__':
    args = _parse_args()
    classifier_constructor = _get_classifier_constructor(args.classifier_class)
    df = pd.read_csv(args.dataset)
    clf = classifier_constructor()
    clf.fit([LabeledText(text=text, label=label)
             for text, label in zip(list(df['text']), list(df['label']))])
    with open(args.output_path, 'w') as target:
        json.dump(clf.config, target)