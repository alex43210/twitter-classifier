import json
import os
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.utils import compute_sample_weight
from unittest import TestCase
import numpy as np
import gc
from .tweet_classifier import TweetClassifier
from .base_classifier import LabeledText


class TwitterClassifierTest(TestCase):
    def _readDataset(self):
        def _labeled_text_item(text, label):
            if label == 2:
                label = 1
            elif label == -2:
                label = -1
            return LabeledText(text=text, label=label)

        dataset_path = os.path.join(os.path.dirname(__file__), 'twitter_classifier_test.json')
        with open(dataset_path, 'rb') as src:
            dataset = json.load(src)
        labeled_texts = [_labeled_text_item(**item) for item in dataset]
        return labeled_texts

    def _loadSplittedDataset(self):
        dataset = self._readDataset()
        texts = np.array([item.text for item in dataset])
        polarity = np.array([item.label for item in dataset])
        train_texts, test_texts, train_polarity, test_polarity = train_test_split(texts, polarity,
                                                                                  test_size=500,
                                                                                  random_state=42)
        train_dataset = [LabeledText(text, label) for text, label in zip(train_texts, train_polarity)]
        return train_dataset, test_texts, test_polarity

    def _accuracy(self, y_true, y_pred):
        return accuracy_score(y_true, y_pred,
                              sample_weight=compute_sample_weight('balanced', y_true))

    def testClassifier(self):
        train_dataset, test_texts, test_polarity = self._loadSplittedDataset()
        classifier = TweetClassifier()
        classifier.fit(train_dataset)
        prediction = classifier.predict(test_texts)
        accuracy = self._accuracy(test_polarity, prediction)
        self.assertGreater(accuracy, 0.48)

    def testClassifierInstantiation(self):
        train_dataset, test_texts, test_polarity = self._loadSplittedDataset()
        classifier1 = TweetClassifier()
        config = classifier1.fit(train_dataset).config
        prediction1 = classifier1.predict(test_texts)
        del classifier1
        gc.collect()
        classifier2 = TweetClassifier(**config)
        prediction2 = classifier2.predict(test_texts)
        same_predictions = np.sum(prediction1 == prediction2) == len(prediction1)
        self.assertTrue(same_predictions)
