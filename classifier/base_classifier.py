from collections import namedtuple


LabeledText = namedtuple('LabeledText', ['text', 'label'])


class BaseClassifier:
    def __init__(self, **config):
        self._initialize_config(config)

    @property
    def config(self):
        return self._get_config()

    def _initialize_config(self, config):
        raise NotImplementedError("Child must implement BaseClassifier._initialize_config")

    def _get_config(self):
        raise NotImplementedError("Child must implement BaseClassifier._get_config")

    def fit(self, texts):
        """
        Fit classifier
        :param texts: labeled texts
        :type texts: list[LabeledText]
        :return: fitted classifier
        :rtype: BaseClassifier
        """
        return self._fit(texts)

    def _fit(self, texts):
        raise NotImplementedError("Child must implement BaseClassifier._fit")

    def predict(self, texts):
        """
        Predict labels for texts
        :param texts: texts
        :type texts: list[str]
        :return: labels of top-classes
        :rtype: list[str]
        """
        return self._predict(texts)

    def _predict(self, texts):
        raise NotImplementedError("Child must implement BaseClassifier._predict")
