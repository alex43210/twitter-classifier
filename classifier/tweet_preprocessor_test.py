from unittest import TestCase
from .tweet_preprocessor import clean_tweet, clean_tweets


class CleanTweetTest(TestCase):
    def testCleanTweet(self):
        text = "Credit markets aren't doing as well as stocks https://t.co/eucSWvvmaR via @TheStreet $hyg $jnk #CDS"
        self.assertEqual(clean_tweet(text), 'credit market are notdo as well as stock via')

    def testCleanTweets(self):
        text = ["Credit markets aren't doing as well as stocks "
                "https://t.co/eucSWvvmaR via @TheStreet $hyg $jnk #CDS",
                "Does that matter? Fed strangling vol, QE anchoring DM yields, oil in a range, "
                "China burst far away, $EM yields fine https://t.co/4ldFZBrbuE"]
        self.assertEqual(clean_tweets(text), ['credit market are notdo as well as stock via',
                                              'doe that matter ? fed strangl vol , qe anchor dm yield , '
                                              'oil in a rang , china burst far away , yield fine'])
