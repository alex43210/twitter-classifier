from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import FunctionTransformer
from sklearn.base import BaseEstimator
import pickle
from base64 import encodebytes, decodebytes
from .base_classifier import BaseClassifier
from .tweet_preprocessor import clean_tweets


class TweetClassifier(BaseClassifier):
    def _initialize_config(self, config):
        if "cleaner" not in config:
            cleaner = clean_tweets
        else:
            cleaner = config["cleaner"]
        if "classifier" not in config:
            classifier = Pipeline([
                ('clean', FunctionTransformer(cleaner, validate=False)),
                ('vec', CountVectorizer(binary=True)),
                ('clf', LogisticRegression(penalty='l1',
                                           class_weight={-1: 2, 0: 0.2, 1: 2}))
            ])
        else:
            classifier = pickle.loads(decodebytes(config['classifier'].encode('ascii')))
        assert isinstance(classifier, BaseEstimator)
        self._classifier = classifier

    def _get_config(self):
        return {
            'classifier': encodebytes(pickle.dumps(self._classifier)).decode('ascii')
        }

    def _fit(self, texts):
        x = [text.text for text in texts]
        y = [text.label for text in texts]
        self._classifier.fit(x, y)
        return self

    def _predict(self, texts):
        return self._classifier.predict(texts)
