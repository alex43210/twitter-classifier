import re
from nltk.tokenize import wordpunct_tokenize
from nltk.stem.snowball import EnglishStemmer
from preprocessor import clean


stemmer = EnglishStemmer()


def clean_tweet(tweet_text):
    """
    Clean text of tweet
    :param tweet_text: source tweet text
    :type tweet_text: str
    :return: cleaned tweet text
    :rtype: str
    """
    links_replaced = re.sub(r'^https?:\/\/.*[\r\n]*', 'LINK', tweet_text, flags=re.MULTILINE)
    nt_to_not = links_replaced.lower().replace('n\'t', ' not ')
    cleaned_tweet = clean(re.sub('\$\w+', '', nt_to_not))
    tokens = wordpunct_tokenize(cleaned_tweet)
    stemmed_text = ' '.join(map(stemmer.stem, tokens)).replace(' not ', ' not')
    return stemmed_text.replace(' not ', 'not ')


def clean_tweets(tweet_texts):
    return [clean_tweet(text) for text in tweet_texts]
