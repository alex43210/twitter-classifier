There you can see batch text classifier module for stock tweet processor.
It requires:

- python3
- tweet-preprocessor==0.5.0
- scikit-learn==0.19.0
- numpy==1.13.1
- flask==0.12.2
- nltk==3.2.5

Packages can be installed via pip:

```
pip3 install -r requirements.txt
```

Usage
-----
You'll need to run classifier server. You can make it via app.py:

    python3 app.py --classifier_class classifierClassName --classifier_config_path classifierConfigPath --port portNumber --host hostName --debug debugFlag
    
Where:

- classifier_class - full name of classifier class (by default - ```classifier.TweetClassifier```)
- classifier_config_path - path to trained classifier configuration file (by default - ```classifier.json```)
- port - port to listen via Flask
- host - host to listen via Flask
- debug - Flask debug flag

Next you'll need to make request to classifier. E.g. we have 2 texts:

- Does that matter? Fed strangling vol, QE anchoring DM yields, oil in a range, China burst far away, $EM yields fine https://t.co/4ldFZBrbuE
- What's Next For Bond Yields? via @forbes https://t.co/GVKKhbR318 $TLT $TBT $IEF $LQD - look for bonds to rally next - too far, too fast

So request will be:

    curl -X POST -d "text[]=Does that matter? Fed strangling vol, QE anchoring DM yields, oil in a range, China burst far away, $EM yields fine https://t.co/4ldFZBrbuE&text[]=What's Next For Bond Yields? via @forbes https://t.co/GVKKhbR318 $TLT $TBT $IEF $LQD - look for bonds to rally next - too far, too fast" http://127.0.0.1:8080/classify
    
And answer in default config:

    [0, 1]
    
Where each value - classification of one text (for default classifier - -1 is negative, 0 is neutral and 1 is positive).

So classification result is next:

- ```(NEUTRAL)``` Does that matter? Fed strangling vol, QE anchoring DM yields, oil in a range, China burst far away, $EM yields fine https://t.co/4ldFZBrbuE
- ```(POSITIVE)``` What's Next For Bond Yields? via @forbes https://t.co/GVKKhbR318 $TLT $TBT $IEF $LQD - look for bonds to rally next - too far, too fast
