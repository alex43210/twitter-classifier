from flask import Flask, request
from classifier import BaseClassifier
from argparse import ArgumentParser
import json
from importlib import import_module
import os


def _parse_args():
    parser = ArgumentParser()
    parser.add_argument("--classifier_class",
                        help="text classifier class name",
                        default="classifier.TweetClassifier")
    parser.add_argument("--classifier_config_path",
                        help="path to classifier configuration file (must be JSON)",
                        default=os.path.join(os.path.dirname(__file__), 'classifier.json'))
    parser.add_argument("--port",
                        help="port number to listen",
                        default=8080,
                        type=int)
    parser.add_argument("--host",
                        help="host to listen",
                        default='127.0.0.1')
    parser.add_argument("--debug",
                        help="debug flag",
                        default=False,
                        type=bool)
    return parser.parse_args()


def _get_classifier_constructor(classifier_full_class_name):
    classifier_module_name = '.'.join(classifier_full_class_name.split('.')[:-1])
    classifier_class_name = classifier_full_class_name.split('.')[-1]
    classifier_module = import_module(classifier_module_name)
    classifier_class = getattr(classifier_module, classifier_class_name)
    return classifier_class


def _get_classifier_config(classifier_config_path):
    with open(classifier_config_path, 'r') as src:
        return json.load(src)


if __name__ == '__main__':
    args = _parse_args()
    classifier_constructor = _get_classifier_constructor(args.classifier_class)
    classifier_config = _get_classifier_config(args.classifier_config_path)
    classifier = classifier_constructor(**classifier_config)
    assert isinstance(classifier, BaseClassifier)
    app = Flask(__name__)

    @app.route('/classify', methods=['POST'])
    def classify():
        texts = request.form.getlist('text[]')
        labels = list(map(int, classifier.predict(texts)))
        return json.dumps(labels)

    app.run(args.host, args.port, args.debug)
